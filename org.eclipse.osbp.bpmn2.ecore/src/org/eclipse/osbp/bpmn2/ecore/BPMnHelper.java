/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpmn2.ecore;

import org.eclipse.bpmn2.Assignment;
import org.eclipse.bpmn2.DataInput;
import org.eclipse.bpmn2.DataInputAssociation;
import org.eclipse.bpmn2.Task;
import org.eclipse.bpmn2.impl.FormalExpressionImpl;

public class BPMnHelper {

	static public String getBpmTaskLocale(Task task) {
		return getBpmTaskDataInputProperty(task, "Locale");
	}

	static public String getBpmTaskGroupId(Task task) {
		return getBpmTaskDataInputProperty(task, "GroupId");
	}

	static public String getBpmTaskDataInputProperty(Task task, String property) {
		String dataInputId = null;
		try {
			for (DataInput dataInput : task.getIoSpecification().getDataInputs()) {
				if	(property.equals(dataInput.getName())) {
					dataInputId = dataInput.getId();
				}
			}
			if	(dataInputId != null) {
				for	(DataInputAssociation association : task.getDataInputAssociations()) {
					for	(Assignment assignment : association.getAssignment()) {
						if	(dataInputId.equals(((FormalExpressionImpl)assignment.getTo()).getMixed().iterator().next().getValue())) {
							return ((FormalExpressionImpl)assignment.getFrom()).getMixed().iterator().next().getValue().toString();
						}
					}
				}
			}
		}
		catch (Exception e) {
			System.err.println();
		}
		return null;
	}

}
