/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     Sebastian Zarnekow - Initial contribution and API
 *     Christophe Loetz (Loetz GmbH&Co.KG) - integration in and adaption to OSBP project                                                       
 *******************************************************************************/

package org.eclipse.osbp.bpmn2.ecore;

/**
 * Can be used to cause a side effect in workflow. As soon as this bean
 * is instantiated in a workflow, the {@link org.eclipse.xtext.resource.IResourceServiceProvider services}
 * for {@code *.ecore} files are registered. Existing services will not be replaced.
 * @author Sebastian Zarnekow - Initial contribution and API
 */
public class BPMnSupportStandaloneSetup {

	public static void setup() {
		new BPMnSupportStandaloneSetup();
	}
	
	public BPMnSupportStandaloneSetup() {
		BPMnSupport bpmnSupport = new BPMnSupport();
		bpmnSupport.registerServices(false);
		bpmnSupport.createInjectorAndDoEMFRegistration();
	}
	
}
