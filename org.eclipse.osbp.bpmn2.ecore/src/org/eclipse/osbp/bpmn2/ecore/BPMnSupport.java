/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     Sven Efftinge - Initial contribution and API
 *     Christophe Loetz (Loetz GmbH&Co.KG) - integration in and adaption to OSBP project                                                       
 *******************************************************************************/

package org.eclipse.osbp.bpmn2.ecore;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * 
 * This class is intended to be used from an MWE workflow.
 * 
 * It instantiates and registers the Ecore support for Xtext, which allows for
 * referencing ecore models from any Xtext language.
 * 
 * Usage:
 * 
 * <pre>
 *    component = org.eclipse.xtext.ecore.EcoreSupport{}
 * </pre>
 * 
 * If you want to provide a different guice guiceModule than the default one (
 * {@link BPMnRuntimeModule}) in order to change any implementation classes, you
 * can make use of the property guiceModule. E.g. :
 * 
 * <pre>
 *    component = org.eclipse.xtext.ecore.EcoreSupport{
 *       guiceModule = my.special.CustomizedEcoreRuntimeModule {}
 *    }
 * </pre>
 * 
 * @author Sven Efftinge - Initial contribution and API
 */
public class BPMnSupport extends AbstractGenericResourceSupport implements
		ISetup {

	@Override
	protected Module createGuiceModule() {
		return new BPMnRuntimeModule();
	}

	/**
	 * @since 2.5
	 */
	public Injector createInjectorAndDoEMFRegistration() {
		Injector injector = Guice.createInjector(getGuiceModule());
		injector.injectMembers(this);
		registerInRegistry(false);

		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
				"bpmn2", new BPMnResourceFactoryImpl());

		return injector;
	}
}
