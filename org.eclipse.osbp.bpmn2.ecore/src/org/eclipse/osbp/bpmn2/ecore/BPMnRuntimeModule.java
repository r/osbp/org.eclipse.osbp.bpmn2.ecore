/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     Jan Koehnlein - Initial contribution and API
 *     Christophe Loetz (Loetz GmbH&Co.KG) - integration in and adaption to OSBP project                                                       
 *******************************************************************************/

package org.eclipse.osbp.bpmn2.ecore;

import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;
import org.eclipse.xtext.resource.generic.GenericResourceDescriptionManager;

/**
 * Default Guice bindings for managing Ecore resources in the context of Xtext.
 *  
 * @author Jan Koehnlein - Initial contribution and API
 */
public class BPMnRuntimeModule extends AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "org.eclipse.bpmn2.modeler.ui.bpmn2editor";
	}

	@Override
	protected String getFileExtensions() {
		return "bpmn2";
	}
	
//	public Class<? extends IResourceDescription.Manager> bindIResourceDescription$Manager() {
//		return BPMnResourceDescriptionManager.class;
//	}
	
	public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return BPMnResourceDescriptionStrategy.class;
	}
	
	@Override
	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return BPMnQualifiedNameProvider.class;
	}
	
}
