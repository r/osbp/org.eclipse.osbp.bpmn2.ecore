/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.bpmn2.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebServiceUtilities {
	private final static Logger log = LoggerFactory.getLogger(WebServiceUtilities.class);
	
	public void invoke() {
		log.error("invoking of web service, which was not modelled inside blip");
	}
}
