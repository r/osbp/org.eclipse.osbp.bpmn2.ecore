/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpmn2.ecore.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.ui.resource.IResourceSetInitializer;
import org.eclipse.osbp.bpmn2.ecore.BPMnResourceFactoryImpl;

public class BPMnResourceSetInitializer implements IResourceSetInitializer {

	public void initialize(ResourceSet resourceSet, IProject project) {
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("bpmn2", new BPMnResourceFactoryImpl());
	}

}
