/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.bpmn2.ecore.ui;

import org.eclipse.bpmn2.Documentation;
import org.eclipse.bpmn2.Task;
import org.eclipse.bpmn2.UserTask;
import org.eclipse.bpmn2.impl.BaseElementImpl;
import org.eclipse.bpmn2.impl.EndEventImpl;
import org.eclipse.bpmn2.impl.ProcessImpl;
import org.eclipse.bpmn2.impl.StartEventImpl;
import org.eclipse.bpmn2.impl.TaskImpl;
import org.eclipse.bpmn2.impl.UserTaskImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.bpmn2.ecore.BPMnHelper;
import org.eclipse.xtext.ui.editor.hover.html.DefaultHoverDocumentationProvider;

public class BPMnEObjectHoverDocumentationProvider extends DefaultHoverDocumentationProvider {

	public BPMnEObjectHoverDocumentationProvider() {
		super();
	}

	public static String getDocumentation(BaseElementImpl element) {
		String documentation = "";
		if	(element instanceof UserTaskImpl) {
			documentation += "<b>User task:</b> <i>"+((UserTaskImpl)element).getName()+"</i><br>";
			documentation += "- id:"+((UserTaskImpl)element).getId()+"<br>";
			documentation += "- Group Id:"+BPMnHelper.getBpmTaskGroupId((UserTask)element)+"<br>";
			documentation += "- Locale:"+BPMnHelper.getBpmTaskLocale((UserTask)element)+"<br>";
		}
		else if	(element instanceof TaskImpl) {
			documentation += "<b>Task:</b> <i>"+((TaskImpl)element).getName()+"</i><br>";
			documentation += "- id:"+((TaskImpl)element).getId()+"<br>";
			documentation += "- Group Id:"+BPMnHelper.getBpmTaskGroupId((Task)element)+"<br>";
			documentation += "- Locale:"+BPMnHelper.getBpmTaskLocale((Task)element)+"<br>";
		}
		else if	(element instanceof StartEventImpl) {
			documentation += "<b>Start Event:</b> <i>"+((StartEventImpl)element).getName()+"</i> id:"+((StartEventImpl)element).getId()+"<br>";
		}
		else if	(element instanceof EndEventImpl) {
			documentation += "<b>End Event:</b> <i>"+((EndEventImpl)element).getName()+"</i> id:"+((EndEventImpl)element).getId()+"<br>";
		}
		else if (element instanceof ProcessImpl) {
			documentation += "<b>Process:</b> <i>"+((ProcessImpl)element).getName()+"</i> id:"+((ProcessImpl)element).getId()+"<br>";
		}
		for (Documentation docItem : element.getDocumentation()) {
			if	(!docItem.getText().isEmpty()) {
				documentation += docItem.getText()+"<br>";
			}
		}
		if	(element instanceof UserTaskImpl) {
			documentation += "<br>"+getDocumentation((BaseElementImpl)((UserTaskImpl)element).eContainer());
		}
		else if	(element instanceof TaskImpl) {
			documentation += "<br>"+getDocumentation((BaseElementImpl)((TaskImpl)element).eContainer());
		}
		else if	(element instanceof StartEventImpl) {
			documentation += "<br>"+getDocumentation((BaseElementImpl)((StartEventImpl)element).eContainer());
		}
		else if	(element instanceof EndEventImpl) {
			documentation += "<br>"+getDocumentation((BaseElementImpl)((EndEventImpl)element).eContainer());
		}
		return documentation;
	}
	
	@Override
	public String getDocumentation(EObject object) {
		String documentation = "";
		if	(object instanceof BaseElementImpl) {
			documentation += getDocumentation((BaseElementImpl) object);
		}
		String superDocumentation = super.getDocumentation(object);
		if	(superDocumentation != null) {
			documentation += superDocumentation;
		}
		return documentation;
	}
	
}
